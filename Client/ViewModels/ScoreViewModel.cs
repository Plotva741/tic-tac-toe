﻿using ReactiveUI;

namespace Client.ViewModels
{
    public class ScoreViewModel : ReactiveObject
    {
        private int _player;
        private int _opponent;

        public int Opponent
        {
            get => _opponent;
            set => this.RaiseAndSetIfChanged(ref _opponent, value);
        }
        public int Player
        {
            get => _player;
            set => this.RaiseAndSetIfChanged(ref _player, value);
        }
    }
}