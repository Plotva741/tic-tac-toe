﻿using System.Collections.Generic;
using System.Reactive;
using System.Threading.Tasks;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Windows.Input;
using Client.Interfaces;

namespace Client.ViewModels
{
    public class GamesViewModel : ReactiveObject, IRoutableViewModel
    {
        private readonly ISignalRClient _signalRClient;

        public string UrlPathSegment { get; } = "Games";
        public IScreen HostScreen { get; }

        [Reactive] public List<string> Games { get; set; }
        public string SelectedGame { get; set; }

        public ReactiveCommand<Unit, Unit> JoinGameCommand { get; }
        public ReactiveCommand<MouseButtonEventArgs, Unit> ClickGameCommand { get; }
        public ReactiveCommand<Unit, Unit> LoadGamesCommand { get; }
        public ReactiveCommand<Unit, Unit> BackCommand { get; }

        public GamesViewModel(ISignalRClient signalRClient = default, IScreen screen = default)
        {
            _signalRClient = signalRClient ?? Locator.Current.GetService<ISignalRClient>();
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();

            LoadGamesCommand = ReactiveCommand.CreateFromTask(LoadGamesAsync);
            JoinGameCommand = ReactiveCommand.Create(JoinGame);
            ClickGameCommand = ReactiveCommand.Create<MouseButtonEventArgs>(args => JoinGame());
            BackCommand = ReactiveCommand.Create(NavigateBack);

            LoadGamesCommand.Execute().Subscribe(_ => { });
        }

        private async Task LoadGamesAsync()
        {
            var games = await _signalRClient.GetAvailableGamesAsync();

            Games = new List<string>(games);
        }

        private void JoinGame()
        {
            HostScreen.Router.Navigate.Execute(new GameViewModel(SelectedGame, joined: true));
        }

        private void NavigateBack()
        {
            HostScreen.Router.NavigateBack.Execute();
        }
    }
}