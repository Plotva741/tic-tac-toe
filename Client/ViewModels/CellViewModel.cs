﻿using Client.Entities;
using ReactiveUI;

namespace Client.ViewModels
{
    public class CellViewModel : ReactiveObject
    {
        private Position _position;
        private State _state;

        public Position Position
        {
            get => _position;
            set => this.RaiseAndSetIfChanged(ref _position, value);
        }

        public State State
        {
            get => _state;
            set => this.RaiseAndSetIfChanged(ref _state, value);
        }
    }
}