﻿using System.Reactive;
using ReactiveUI;
using Splat;

namespace Client.ViewModels
{
    public class MenuViewModel : ReactiveObject, IRoutableViewModel
    {
        public string UrlPathSegment => "Menu";

        public IScreen HostScreen { get; }

        public ReactiveCommand<Unit, Unit> NewGameCommand { get; }
        public ReactiveCommand<Unit, Unit> GetGamesCommand { get; }

        public MenuViewModel(IScreen screen = default)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();

            NewGameCommand = ReactiveCommand.Create(ShowCreateGameView);
            GetGamesCommand = ReactiveCommand.Create(GetAvailableGamesAsync);
        }
        private void ShowCreateGameView()
        {
            HostScreen.Router.Navigate.Execute(new CreateGameViewModel(screen: HostScreen));
        }

        private void GetAvailableGamesAsync()
        {
            HostScreen.Router.Navigate.Execute(new GamesViewModel(screen: HostScreen));
        }
    }
}
