﻿using System.Reactive;
using System.Threading.Tasks;
using Client.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;

namespace Client.ViewModels
{
    public class CreateGameViewModel : ReactiveObject, IRoutableViewModel
    {
        private readonly ISignalRClient _signalRClient;

        public string UrlPathSegment => "CreateGame";
        public IScreen HostScreen { get; }

        [Reactive] public string Game { get; set; }
        [Reactive] public bool GameExists { get; set; }

        public ReactiveCommand<Unit, Unit> BackCommand { get; }
        public ReactiveCommand<Unit, Unit> CreateCommand { get; }

        public CreateGameViewModel(ISignalRClient signalRClient = default, IScreen screen = default)
        {
            _signalRClient = signalRClient ?? Locator.Current.GetService<ISignalRClient>();
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();

            BackCommand = ReactiveCommand.Create(NavigateBack);
            CreateCommand = ReactiveCommand.CreateFromTask(CreateGameAsync,
                this.WhenAnyValue(vm => vm.Game, r => !string.IsNullOrWhiteSpace(r)));
        }

        private async Task CreateGameAsync()
        {
            var gameExists = await _signalRClient.GameExistsAsync(Game);
            if (gameExists)
            {
                GameExists = true;
            }
            else
            {
                HostScreen.Router.Navigate.Execute(new GameViewModel(Game, joined: false));
            }
        }

        private void NavigateBack()
        {
            HostScreen.Router.NavigateBack.Execute();
        }
    }
}
