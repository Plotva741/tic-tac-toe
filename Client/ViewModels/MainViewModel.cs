﻿using System.Reactive;
using System.Threading.Tasks;
using System.Windows;
using Client.Interfaces;
using ReactiveUI;
using Splat;

namespace Client.ViewModels
{
    public class MainViewModel : ReactiveObject, IScreen
    {
        public RoutingState Router { get; }

        private readonly ISignalRClient _signalRClient;

        public ReactiveCommand<RoutedEventArgs, Unit> InitCommand { get; }

        public MainViewModel(ISignalRClient signalRClient = default)
        {
            _signalRClient = signalRClient ?? Locator.Current.GetService<ISignalRClient>();
            Router = new RoutingState();

            InitCommand = ReactiveCommand.CreateFromTask<RoutedEventArgs>(async args => await InitAsync());
        }

        private async Task InitAsync()
        {
            await _signalRClient.ConnectAsync();

            Router.Navigate.Execute(new MenuViewModel(this));
        }
    }
}