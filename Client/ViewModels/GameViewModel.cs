﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using Client.Entities;
using Client.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;

namespace Client.ViewModels
{
    public class GameViewModel : ReactiveObject, IRoutableViewModel
    {
        private readonly IGameRulesService _gameRulesService;
        private IDisposable _opponentJoined;

        public string UrlPathSegment { get; } = "Game";
        public IScreen HostScreen { get; }

        [Reactive] public bool IsPlayersTurn { get; set; }
        [Reactive] public bool GameIsGoing { get; set; }
        [Reactive] public Winner Winner { get; set; }

        public string Game { get; set; }
        public ScoreViewModel Score { get; set; }
        public IEnumerable<CellViewModel> Cells { get; }

        public ReactiveCommand<CellViewModel, Unit> MakeTurnCommand { get; }
        public ReactiveCommand<Unit, Unit> RestartCommand { get; set; }
        public ReactiveCommand<RoutedEventArgs, Unit> InitCommand { get; }


        public GameViewModel(string game, bool joined, IGameRulesService gameRulesService = default, IScreen screen = default)
        {
            _gameRulesService = gameRulesService ?? Locator.Current.GetService<IGameRulesService>();
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();

            Game = game;
            Cells = _gameRulesService.Cells;
            Score = new ScoreViewModel();
            if (joined)
            {
                GameIsGoing = true;
            }

            var canExecute = this.WhenAnyValue(vm => vm.IsPlayersTurn, vm => vm.GameIsGoing, (a, b) => a && b);
            MakeTurnCommand = ReactiveCommand.CreateFromTask<CellViewModel>(MakeTurnAsync, canExecute);
            RestartCommand = ReactiveCommand.Create(Restart);
            InitCommand = ReactiveCommand.CreateFromTask<RoutedEventArgs>(async args => await StartAsync());

            InitCommand.Execute().Subscribe(_ => { });
        }

        private async Task StartAsync()
        {
            await _gameRulesService.StartAsync(Game);

            _gameRulesService.WhenOpponentTurnReceived
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(OpponentTurnReceived);

            _gameRulesService.WhenOpponentWon
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(_ => OpponentWon());

            _opponentJoined = _gameRulesService.WhenOpponentJoined
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(_ => OpponentJoined());

            IsPlayersTurn = _gameRulesService.PlayerSign == State.Cross;
        }

        private void OpponentJoined()
        {
            GameIsGoing = true;

            _opponentJoined.Dispose();
        }

        private void Restart()
        {
            Winner = Winner.None;
            GameIsGoing = true;
            _gameRulesService.Reset();
        }

        private void OpponentWon()
        {
            Winner = Winner.Opponent;
            GameIsGoing = false;
            Score.Opponent++;
        }

        private void OpponentTurnReceived(Cell cell)
        {
            var oldCell = Cells.FirstOrDefault(c => c.Position == cell.Position);
            if (oldCell != null)
            {
                oldCell.State = cell.State;
                IsPlayersTurn = true;
            }
        }

        private async Task MakeTurnAsync(CellViewModel cell)
        {
            if (cell.State == State.Blank)
            {
                IsPlayersTurn = false;

                var isWinner = await _gameRulesService.MakeTurnAsync(cell);
                if (isWinner)
                {
                    Score.Player++;
                    Winner = Winner.Player;
                    GameIsGoing = false;
                }
            }
        }
    }
}