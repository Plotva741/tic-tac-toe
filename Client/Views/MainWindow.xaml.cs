﻿using System.Reactive.Disposables;
using System.Windows.Controls;
using Client.ViewModels;
using ReactiveUI;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ReactiveWindow<MainViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();

            ViewModel = new MainViewModel();

            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel, vm => vm.Router, v => v.RoutedViewHost.Router)
                    .DisposeWith(disposable);

                RoutedViewHost.Events().Loaded.InvokeCommand(this, v => v.ViewModel.InitCommand)
                    .DisposeWith(disposable);
            });
        }
    }
}