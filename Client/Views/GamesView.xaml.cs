﻿using System.Reactive.Disposables;
using System.Windows.Controls;
using Client.ViewModels;
using ReactiveUI;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for GamesView.xaml
    /// </summary>
    public partial class GamesView : ReactiveUserControl<GamesViewModel>
    {
        public GamesView()
        {
            InitializeComponent();

            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel, vm => vm.Games, v => v.Games.ItemsSource)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel, vm => vm.BackCommand, v => v.BackButton)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel, vm => vm.JoinGameCommand, v => v.JoinGameButton)
                    .DisposeWith(disposable);

                this.WhenAnyValue(x => x.Games.SelectedItem)
                    .BindTo(this, x => x.ViewModel.SelectedGame);

                Games.Events().MouseDoubleClick.InvokeCommand(ViewModel, vm => vm.ClickGameCommand)
                    .DisposeWith(disposable);

                this.Events().Loaded.InvokeCommand(ViewModel, vm => vm.LoadGamesCommand)
                    .DisposeWith(disposable);
            });

            Loaded += (sender, args) => Games.DataContext = ViewModel;
        }
    }
}
