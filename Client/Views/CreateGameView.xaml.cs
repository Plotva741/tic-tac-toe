﻿using System.Reactive.Disposables;
using Client.ViewModels;
using ReactiveUI;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for CreateGameView.xaml
    /// </summary>
    public partial class CreateGameView : ReactiveUserControl<CreateGameViewModel>
    {
        public CreateGameView()
        {
            InitializeComponent();

            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel, vm => vm.GameExists, v => v.Error.Visibility);

                this.Bind(ViewModel, vm => vm.Game, v => v.GameTextBox.Text)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel, vm => vm.BackCommand, v => v.BackButton)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel, vm => vm.CreateCommand, v => v.CreateButton)
                    .DisposeWith(disposable);
            });
        }
    }
}
