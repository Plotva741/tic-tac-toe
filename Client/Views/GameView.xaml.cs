﻿using System.Reactive.Disposables;
using System.Windows;
using System.Windows.Controls;
using Client.Entities;
using Client.ViewModels;
using ReactiveUI;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for GameView.xaml
    /// </summary>
    public partial class GameView : ReactiveUserControl<GameViewModel>
    {
        public GameView()
        {
            InitializeComponent();

            this.WhenActivated(disposable =>
            {
                this.BindCommand(ViewModel, vm => vm.RestartCommand, v => v.RestartButton)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel, vm => vm.Score.Player, v => v.PlayerScoreTextBlock.Text)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel, vm => vm.Score.Opponent, v => v.OpponentScoreTextBlock.Text)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel, vm => vm.Cells, v => v.Cells.ItemsSource)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel, vm => vm.GameIsGoing, v => v.GameEndedPanel.Visibility,
                        g => g ? Visibility.Collapsed : Visibility.Visible)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel, vm => vm.Winner, v => v.WinnerTextBlock.Text, ConvertWinnerToString)
                    .DisposeWith(disposable);

                this.Events().Loaded.InvokeCommand(ViewModel, vm => vm.InitCommand)
                    .DisposeWith(disposable);
            });

            Loaded += (sender, args) => Cells.DataContext = ViewModel;
        }

        private string ConvertWinnerToString(Winner winner)
        {
            switch (winner)
            {
                case Winner.Player:
                    return "You won!";
                case Winner.Opponent:
                    return "You lost!";
                default:
                    return null;
            }
        }
    }
}
