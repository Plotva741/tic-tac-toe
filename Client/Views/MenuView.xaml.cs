﻿using System.Reactive.Disposables;
using Client.ViewModels;
using ReactiveUI;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for MenuView.xaml
    /// </summary>
    public partial class MenuView : ReactiveUserControl<MenuViewModel>
    {
        public MenuView()
        {
            InitializeComponent();

            this.WhenActivated(disposable =>
            {
                this.BindCommand(ViewModel, vm => vm.NewGameCommand, v => v.NewGameButton)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel, vm => vm.GetGamesCommand, v => v.GetGamesButton)
                    .DisposeWith(disposable);
            });
        }
    }
}
