using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Client.Entities;

namespace Client.Converters
{
    public class StateToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is State state)
            {
                switch (state)
                {
                    case State.Cross:
                        return new BitmapImage(new Uri(@"/Images/cross.png", UriKind.Relative));
                    case State.Circle:
                        return new BitmapImage(new Uri(@"/Images/circle.png", UriKind.Relative));
                    default:
                        return null;
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}