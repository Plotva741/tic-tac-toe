﻿namespace Client.Entities
{
    public enum Winner
    {
        None,
        Player,
        Opponent
    }
}