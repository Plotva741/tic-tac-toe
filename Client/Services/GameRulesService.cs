﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
using Client.Entities;
using Client.Interfaces;
using Client.ViewModels;
using Splat;

namespace Client.Services
{
    public class GameRulesService : IGameRulesService
    {
        private readonly ISignalRClient _signalRClient;

        private readonly int[][] _rows =
        {
            new[] {0, 1, 2},
            new[] {3, 4, 5},
            new[] {6, 7, 8}
        };

        private readonly int[][] _columns =
        {
            new[] {0, 3, 6},
            new[] {1, 4, 7},
            new[] {2, 5, 8}
        };

        private readonly int[][] _diagonals =
        {
            new[] {0, 4, 8},
            new[] {2, 4, 6}
        };

        public State PlayerSign { get; private set; }
        public List<CellViewModel> Cells { get; } = new List<CellViewModel>
        {
            new CellViewModel {Position = Position.TopLeft},
            new CellViewModel {Position = Position.Top},
            new CellViewModel {Position = Position.TopRight},
            new CellViewModel {Position = Position.Left},
            new CellViewModel {Position = Position.Center},
            new CellViewModel {Position = Position.Right},
            new CellViewModel {Position = Position.BottomLeft},
            new CellViewModel {Position = Position.Bottom},
            new CellViewModel {Position = Position.BottomRight}
        };

        public IObservable<Cell> WhenOpponentTurnReceived { get; set; }
        public IObservable<Unit> WhenOpponentWon { get; set; }
        public IObservable<Unit> WhenOpponentJoined { get; set; }

        public GameRulesService(ISignalRClient signalRClient = default)
        {
            _signalRClient = signalRClient ?? Locator.Current.GetService<ISignalRClient>();
        }

        public async Task StartAsync(string game)
        {
            await _signalRClient.ConnectAsync().ConfigureAwait(false);

            WhenOpponentTurnReceived = _signalRClient.WhenOpponentTurnReceived;
            WhenOpponentWon = _signalRClient.WhenOpponentWon;
            WhenOpponentJoined = _signalRClient.WhenOpponentJoined;

            PlayerSign = await _signalRClient.JoinGroupAsync(game).ConfigureAwait(false);
        }

        public async Task<bool> MakeTurnAsync(CellViewModel oldCell)
        {
            oldCell.State = PlayerSign;

            var cell = new Cell { Position = oldCell.Position, State = oldCell.State };

            await _signalRClient.SendTurnAsync(cell).ConfigureAwait(false);

            if (CheckIfWon(oldCell.Position))
            {
                await _signalRClient.AnnounceWinnerAsync().ConfigureAwait(false);

                return true;
            }

            return false;
        }

        private bool CheckIfWon(Position position)
        {
            if (CheckIfWon(_rows, position))
            {
                return true;
            }

            if (CheckIfWon(_columns, position))
            {
                return true;
            }

            if (CheckIfWon(_diagonals, position))
            {
                return true;
            }

            return false;
        }

        private bool CheckIfWon(int[][] cells, Position position)
        {
            var index = (int)position;

            var arr = cells.FirstOrDefault(r => r.Contains(index));

            return arr != null && arr.All(p => Cells[p].State == PlayerSign);
        }

        public void Reset()
        {
            foreach (var cell in Cells)
            {
                cell.State = State.Blank;
            }
        }
    }
}