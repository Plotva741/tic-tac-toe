﻿using System;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using Client.Entities;
using Client.Interfaces;
using Microsoft.AspNetCore.SignalR.Client;

namespace Client.Services
{
    public class SignalRClient : ISignalRClient
    {
        private readonly HubConnection _connection;

        private readonly Subject<Cell> _whenOpponentTurnReceived = new Subject<Cell>();
        private readonly Subject<Unit> _whenOpponentWon = new Subject<Unit>();
        private readonly Subject<Unit> _whenOpponentJoined = new Subject<Unit>();

        public IObservable<Cell> WhenOpponentTurnReceived => _whenOpponentTurnReceived.AsObservable();
        public IObservable<Unit> WhenOpponentWon => _whenOpponentWon.AsObservable();
        public IObservable<Unit> WhenOpponentJoined => _whenOpponentJoined.AsObservable();

        public SignalRClient()
        {
            _connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/game")
                .Build();

            _connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000).ConfigureAwait(false);
                await _connection.StartAsync().ConfigureAwait(false);
            };
        }

        public async Task ConnectAsync()
        {
            if (_connection.State == HubConnectionState.Disconnected)
            {
                _connection.On<Cell>("ReceiveOpponentTurnAsync", cell => _whenOpponentTurnReceived.OnNext(cell));
                _connection.On("AnnounceWinnerAsync", () => _whenOpponentWon.OnNext(Unit.Default));
                _connection.On("NotifyGameJoinedAsync", () => _whenOpponentJoined.OnNext(Unit.Default));

                await _connection.StartAsync().ConfigureAwait(false);
            }
        }

        public async Task SendTurnAsync(Cell cell)
        {
            await _connection.InvokeAsync("SendTurnAsync", cell).ConfigureAwait(false);
        }
        public async Task<State> JoinGroupAsync(string groupName)
        {
            return await _connection.InvokeAsync<State>("JoinGroupAsync", groupName).ConfigureAwait(false);
        }

        public async Task LeaveGroupAsync(string groupName)
        {
            await _connection.InvokeAsync("LeaveGroupAsync", groupName).ConfigureAwait(false);
        }

        public async Task<IEnumerable<string>> GetAvailableGamesAsync()
        {
            return await _connection.InvokeAsync<IEnumerable<string>>("GetAvailableGamesAsync").ConfigureAwait(false);
        }

        public async Task AnnounceWinnerAsync()
        {
            await _connection.InvokeAsync("AnnounceWinnerAsync").ConfigureAwait(false);
        }
        
        public async Task DisconnectAsync()
        {
            await _connection.StopAsync().ConfigureAwait(false);
        }

        public async Task<bool> GameExistsAsync(string game)
        {
            return await _connection.InvokeAsync<bool>("GameExistsAsync", game).ConfigureAwait(false);
        }
    }
}
