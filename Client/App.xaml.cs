﻿using System.Reflection;
using System.Windows;
using Client.Interfaces;
using Client.Services;
using ReactiveUI;
using Splat;

namespace Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Locator.CurrentMutable.RegisterConstant(new SignalRClient(), typeof(ISignalRClient));
            Locator.CurrentMutable.RegisterConstant(new GameRulesService(), typeof(IGameRulesService));
            //Locator.CurrentMutable.RegisterConstant(new MainViewModel(new SignalRClient()), typeof(IScreen));
            Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetCallingAssembly());
        }
    }
}
