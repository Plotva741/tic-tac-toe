﻿using System;
using System.Collections.Generic;
using System.Reactive;
using System.Threading.Tasks;
using Client.Entities;

namespace Client.Interfaces
{
    public interface ISignalRClient
    {
        IObservable<Cell> WhenOpponentTurnReceived { get; }
        IObservable<Unit> WhenOpponentWon { get; }
        IObservable<Unit> WhenOpponentJoined { get; }

        Task SendTurnAsync(Cell cell);
        Task<State> JoinGroupAsync(string groupName);
        Task LeaveGroupAsync(string groupName);
        Task<IEnumerable<string>> GetAvailableGamesAsync();
        Task AnnounceWinnerAsync();
        Task ConnectAsync();
        Task DisconnectAsync();
        Task<bool> GameExistsAsync(string game);
    }
}