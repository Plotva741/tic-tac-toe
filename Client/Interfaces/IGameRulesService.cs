﻿using System;
using System.Collections.Generic;
using System.Reactive;
using System.Threading.Tasks;
using Client.Entities;
using Client.ViewModels;

namespace Client.Interfaces
{
    public interface IGameRulesService
    {
        IObservable<Cell> WhenOpponentTurnReceived { get; set; }
        IObservable<Unit> WhenOpponentWon { get; set; }
        IObservable<Unit> WhenOpponentJoined { get; }
        State PlayerSign { get; }
        List<CellViewModel> Cells { get; }

        Task StartAsync(string game);
        Task<bool> MakeTurnAsync(CellViewModel oldCell);
        void Reset();
    }
}