﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Server.Interfaces;

namespace Server.Services
{
    public class GroupsService : IGroupsService
    {
        private readonly ConcurrentDictionary<string, List<string>> _groups;

        public GroupsService()
        {
            _groups = new ConcurrentDictionary<string, List<string>>();
        }

        public void AddToGroup(string groupName, string userId)
        {
            var group = _groups.GetOrAdd(groupName, new List<string>());
            if (group.Count(s => s == userId) == 0)
            {
                group.Add(userId);
            }
        }

        public void RemoveFromGroup(string userId)
        {
            var group = GetGroup(userId);
            if (group != null)
            {
                _groups[group].Remove(userId);
            }
        }
        
        public IEnumerable<string> GetAvailable()
        {
            return _groups.Where(g => g.Value.Count == 1).Select(g => g.Key);
        }

        public string GetGroup(string userId)
        {
            return _groups.FirstOrDefault(g => g.Value.Contains(userId)).Key;
        }

        public int GetGroupCount(string groupName)
        {
            return _groups.TryGetValue(groupName, out var group) ? group.Count : 0;
        }

        public bool GroupExists(string groupName)
        {
            return _groups.ContainsKey(groupName);
        }
    }
}