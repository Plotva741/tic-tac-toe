﻿namespace Server.Entities
{
    public class Cell
    {
        public Position Position { get; set; }

        public State State { get; set; }
    }
}