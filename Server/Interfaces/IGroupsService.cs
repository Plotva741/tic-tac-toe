﻿using System.Collections.Generic;

namespace Server.Interfaces
{
    public interface IGroupsService
    {
        void AddToGroup(string groupName, string userId);
        void RemoveFromGroup(string userId);
        IEnumerable<string> GetAvailable();
        int GetGroupCount(string groupName);
        string GetGroup(string userId);
        bool GroupExists(string groupName);
    }
}