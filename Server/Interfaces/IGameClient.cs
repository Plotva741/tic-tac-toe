﻿using System.Threading.Tasks;
using Server.Entities;

namespace Server.Interfaces
{
    public interface IGameClient
    {
        Task ReceiveOpponentTurnAsync(Cell cell);

        Task AnnounceWinnerAsync();

        Task NotifyGameJoinedAsync();
    }
}