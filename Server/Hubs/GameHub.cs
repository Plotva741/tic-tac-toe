﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Server.Entities;
using Server.Interfaces;
using Server.Services;

namespace Server.Hubs
{
    public class GameHub : Hub<IGameClient>
    {
        private readonly IGroupsService _groupsService;

        public GameHub(IGroupsService groupsService)
        {
            _groupsService = groupsService;
        }
        public override Task OnDisconnectedAsync(Exception exception)
        {
            _groupsService.RemoveFromGroup(Context.ConnectionId);

            return base.OnDisconnectedAsync(exception);
        }

        public async Task<State> JoinGroupAsync(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

            _groupsService.AddToGroup(groupName, Context.ConnectionId);

            await Clients.OthersInGroup(groupName).NotifyGameJoinedAsync();

            var count = _groupsService.GetGroupCount(groupName);
            switch (count)
            {
                case 1:
                    return State.Cross;
                case 2:
                    return State.Circle;
                default:
                    return State.Blank;
            }
        }

        public async Task LeaveGroupAsync(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);

            _groupsService.RemoveFromGroup(Context.ConnectionId);
        }

        public async Task SendTurnAsync(Cell cell)
        {
            var group = _groupsService.GetGroup(Context.ConnectionId);

            await Clients.OthersInGroup(group).ReceiveOpponentTurnAsync(cell);
        }

        public Task<IEnumerable<string>> GetAvailableGamesAsync()
        {
            return Task.FromResult(_groupsService.GetAvailable());
        }

        public async Task AnnounceWinnerAsync()
        {
            var group = _groupsService.GetGroup(Context.ConnectionId);

            await Clients.OthersInGroup(group).AnnounceWinnerAsync();
        }

        public Task<bool> GameExistsAsync(string groupName)
        {
            return Task.FromResult(_groupsService.GroupExists(groupName));
        }
    }
}